-- lib/supple.lua
--
-- Sandbox (for) Untrusted Procedure Partitioning (in) Lua Engine
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For licence terms, see COPYING
--

--- Sandbox, for Untrusted Procedure Partitioning in Lua, Engine.
--
-- Supple is a mechanism for partitioning untrusted Lua code into a separate
-- process wherein it is run sandboxed from the environment in an attempt to
-- reduce the attack surface to a level at which administrators will be
-- prepared to run arbitrary code from untrusted (or even expected-malicious)
-- third parties.
--
-- * To see how to use supple, start at `supple.host.run`.

local capi = require 'supple.capi'
local request = require 'supple.request'
local objects = require 'supple.objects'
local comms = require 'supple.comms'
local sandbox = require 'supple.sandbox'
local host = require 'supple.host'
local track = require 'supple.track'

local _VERSION = 1
local _ABI = 1

local VERSION = "Supple Version " .. tostring(_VERSION)

return {
   capi = capi,
   request = request,
   objects = objects,
   comms = comms,
   sandbox = sandbox,
   host = host,
   track = track,
   _VERSION = _VERSION,
   VERSION = VERSION,
   _ABI = _ABI,
   ABI = ABI,
}
