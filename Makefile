all: test

LMODULES := supple supple.request supple.objects supple.sandbox \
	    supple.host supple.comms supple.track
CMODULES := supple.capi
MODULES := $(LMODULES) $(CMODULES)
LUA_VER := 5.1

TEST_MODULES := $(MODULES)

PREFIX ?= /usr/local

INST_BASE := $(PREFIX)
WRAPPER_PATH := $(INST_BASE)/lib/supple-sandbox$(LUA_VER)

LINST_ROOT := $(DESTDIR)$(INST_BASE)/share/lua/$(LUA_VER)
CINST_ROOT := $(DESTDIR)$(INST_BASE)/lib/lua/$(LUA_VER)
BINST_ROOT := $(DESTDIR)$(INST_BASE)/lib
LMOD_FILES := $(patsubst %,%.lua,$(subst .,/,$(LMODULES)))
CMOD_FILES := $(patsubst %,%.so,$(subst .,/,$(CMODULES)))
LIB_LUA := -llua$(LUA_VER)

CMOD_TARGETS := $(patsubst %,lib/%.so,$(subst .,/,$(CMODULES)))
CMOD_OBJECTS := $(patsubst %,lib/%.o,$(subst .,/,$(CMODULES)))

.PRECIOUS: $(CMOD_OBJECTS)

LUA_INTERP_NAME := lua$(LUA_VER)
LUA_INTERP_PATH := $(shell which lua$(LUA_VER))

# Override these variables
# to bake LUA_PATH and LUA_CPATH environment variables into the sandbox binary.
# They can't be set at runtime for security reasons.
BAKE_SUPPLE_PATHS := 0
SUPPLE_LUA_PATH :=
SUPPLE_LUA_CPATH :=

ifeq ($(BAKE_SUPPLE_PATHS), 1)
	DEF_BAKE_SUPPLE_PATHS=-DBAKE_SUPPLE_PATHS
else
	DEF_BAKE_SUPPLE_PATHS=
endif

INCS := -I/usr/include/lua$(LUA_VER)
OPT := -O0 -g
WARN := -Wall -Werror
DEFS := -D'LUA_INTERP_NAME="$(LUA_INTERP_NAME)"' \
        -D'LUA_INTERP_PATH="$(LUA_INTERP_PATH)"' \
	-D'SUPPLE_LUA_PATH="$(SUPPLE_LUA_PATH)"' \
	-D'SUPPLE_LUA_CPATH="$(SUPPLE_LUA_CPATH)"' \
	-D'WRAPPER_PATH="$(WRAPPER_PATH)"' $(DEF_BAKE_SUPPLE_PATHS)

CFLAGS := $(INCS) $(OPT) $(WARN) $(DEFS) $(CFLAGS)
LFLAGS := -O1 -g $(LFLAGS)

%.so: %.o
	$(CC) $(LFLAGS) -shared -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -fPIC -o $@ -c $<

build: $(CMOD_TARGETS) wrapper

wrapper: src/wrapper.c
	$(CC) $(LFLAGS) $(CFLAGS) -o $@ $< $(LIB_LUA)

testwrapper: src/wrapper.c
	$(CC) $(LFLAGS) $(CFLAGS) -DTESTING_SUPPLE -o $@ $< $(LIB_LUA)
	-chown root:root $@ && chmod u+s $@
	ls -l $@

COPY_LMOD := sed -e'/START_TEST_SUPPLE/,/END_TEST_SUPPLE/d'

install: build
	mkdir -p $(LINST_ROOT)/supple
	mkdir -p $(CINST_ROOT)/supple
	mkdir -p $(BINST_ROOT)
	cp wrapper $(DESTDIR)$(WRAPPER_PATH)
	if [ "$$(whoami)" = "root" ]; then \
		chown root:root $(DESTDIR)$(WRAPPER_PATH); \
		chmod u+s $(DESTDIR)$(WRAPPER_PATH); \
	fi
	for MOD in $(sort $(LMOD_FILES)); do \
		$(COPY_LMOD) lib/$${MOD} > $(LINST_ROOT)/$${MOD}; \
	done
	for MOD in $(sort $(CMOD_FILES)); do \
		cp lib/$${MOD} $(CINST_ROOT)/$${MOD}; \
	done

ifeq ($(DEBUG),gdb)
GDB := gdb --args
endif

LUA := SUPPLE_TEST_WRAPPER="$(shell pwd)/testwrapper" LUA_PATH="$(shell pwd)/lib/?.lua;$(shell pwd)/extras/luacov/src/?.lua;;" LUA_CPATH="$(shell pwd)/lib/?.so;;" $(GDB) $(LUA_INTERP_PATH)

clean:
	$(RM) luacov.report.out luacov.stats.out
	$(RM) $(CMOD_TARGETS) $(CMOD_OBJECTS)
	$(RM) wrapper testwrapper supple_paths.h
	$(RM) -r html

distclean: clean
	find . -name "*~" -delete

.PHONY: example
example:
	$(LUA) example/simple-example.lua

.PHONY: test
test: build testwrapper
	@$(RM) luacov.stats.out
	@ERR=0; \
	for MOD in $(sort $(TEST_MODULES)); do \
		echo -n "$${MOD}: "; \
		$(LUA) test/test-$${MOD}.lua; \
		test "x$$?" = "x0" || ERR=1; \
	done; \
	$(LUA) extras/luacov/src/bin/luacov -X luacov. -X test. $(TEST_MODULES); \
	exit $$ERR

.PHONY: interactive
interactive: build
	$(LUA) -e'lace=require"supple"' -i

doc:
	@ldoc .

