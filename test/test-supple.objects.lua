-- test/test-supple.lua
--
-- Supple - Tests for the core module
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- Step one, start coverage

pcall(require, 'luacov')

local objects = require 'supple.objects'
local capi = require 'supple.capi'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.give_integral_types()
   assert(objects.give(nil) == nil, "Nil didn't pass through")
   assert(objects.give(true) == true, "True didn't pass through")
   assert(objects.give(false) == false, "False didn't pass through")
   assert(objects.give(123) == 123, "Number didn't pass through")
   assert(objects.give("") == "", "String didn't pass through")
end

function suite.set_name_works()
   local unique_name = tostring({})
   objects.set_name(unique_name)
   local expn = objects.give({})
   assert(expn.tag:find(unique_name), "set_name not passing through")
end

function suite.give_table()
   local tab = {}
   objects.set_name("give_table")
   local expn = objects.give(tab)
   assert(tab ~= expn, "Table passed through")
   assert(expn.tag:match("^give_table:"), "Tag didn't come through")
   local expn2 = objects.give(tab)
   assert(expn.tag == expn2.tag, "Expansion tags did not match")
end

function suite.receive_integral_types()
   assert(objects.receive(nil) == nil, "Nil didn't pass through")
   assert(objects.receive(true) == true, "True didn't pass through")
   assert(objects.receive(false) == false, "False didn't pass through")
   assert(objects.receive(123) == 123, "Number didn't pass through")
   assert(objects.receive("") == "", "String didn't pass through")
end

function suite.receive_function()
   local last_cb = nil
   local function cb(tag, name, ...)
      last_cb = {
	 tag = tag,
	 name = name,
	 args = {...}
      }
      return last_cb
   end
   local unique_id = tostring({})
   objects.set_proc_call(cb)
   local obj = objects.receive {
      tag = unique_id,
      type = "function"
   }
   assert(capi.rawtype(obj) == "userdata", "Not a proxy")
   assert(capi.type(obj) == "function", "Not a proxied function")
   assert(obj("fish") == last_cb, "Call method not propagated")
   assert(type(last_cb) == "table", "Call didn't reach callback")
   assert(last_cb.tag == unique_id, "Tag not propagated")
   assert(last_cb.name == "__call", "__call not propagated")
   assert(last_cb.args[1] == "fish", "args not propagated")
   local obj2 = objects.receive {
      tag = unique_id
   }
   assert(obj == obj2, "Same object should be same object")
end

function suite.receive_one_of_ours()
   local tab = {}
   local ours = objects.give(tab)
   local back = objects.receive({tag=ours.tag})
   assert(tab == back, "Reception of one of ours didn't work")
end

function suite.give_one_of_theirs()
   local function cb(...)
   end
   objects.set_proc_call(cb)
   local unique_id = "give_one_of_theirs:"..tostring({})
   local obj = objects.receive {
      tag = unique_id,
      type = "function"
   }
   local out = objects.give(obj)
   assert(type(out) == "table", "giving a proxy didn't tabulate")
   assert(out.tag == unique_id, "giving back one of theirs wasn't right")
end

function suite.receive_table()
   local last_cb = nil
   local function cb(tag, name, ...)
      last_cb = {
	 tag = tag,
	 name = name,
	 args = {...}
      }
      return last_cb
   end
   local unique_id = tostring({})
   objects.set_proc_call(cb)
   local obj = objects.receive {
      tag = unique_id,
      type = "table"
   }
   assert(capi.rawtype(obj) == "userdata", "Not a proxy")
   assert(capi.type(obj) == "table", "Not a proxied function")
   assert(obj.thingy == last_cb, "Didn't proxy index")
   assert(last_cb.name == "__index", "didn't proxy")
   assert(last_cb.args[1] == "thingy", "proxy wasn't right")
end

function suite.received_object_gc()
   local last_cb = nil
   local function cb(tag, name, ...)
      last_cb = {
	 tag = tag,
	 name = name,
      }
      return last_cb
   end
   local unique_id = tostring({})
   objects.set_proc_call(cb)
   local obj = objects.receive {
      tag = unique_id,
      type = "table"
   }
   assert(capi.rawtype(obj) == "userdata", "Not a proxy")
   assert(capi.type(obj) == "table", "Not a proxied function")
   -- deliberately lose the object
   obj = nil
   collectgarbage()
   assert(last_cb.name == "__gc", "GC didn't go through")
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
