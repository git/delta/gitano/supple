-- test/test-supple.request.lua
--
-- Supple - Tests for the capi module
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- Step one, start coverage

pcall(require, 'luacov')

local request = require 'supple.request'
local objects = require 'supple.objects'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.serialise_error()
   local err = request.error("m","tb")
   assert(err == [[error=true,message="m",traceback="tb"]],
	  "Error did not serialise properly")
end

function suite.serialise_request_simple()
   local req = request.request("TAG", "METH", "arg1", true)
   local tab = assert(loadstring("return {" .. req .. "}"))()
   assert(tab.object == "TAG", "object tag not passed")
   assert(tab.method == "METH", "method name not passed")
   assert(tab.args.n == 2, "argument count not passed")
   assert(tab.args[1] == "arg1", "arg1 not passed")
   assert(tab.args[2] == true, "arg2 not passed")
end

function suite.serialise_request_simple_table()
   local argtab = {}
   local req = request.request("TAG", "METH", argtab)
   local tab = assert(loadstring("return {" .. req .. "}"))()
   assert(tab.object == "TAG", "object tag not passed")
   assert(tab.method == "METH", "method name not passed")
   assert(tab.args.n == 1, "argument count not passed")
   -- Best check that the received arg unwraps nicely
   assert(objects.receive(tab.args[1]) == argtab, "arg did not unwrap")
end

function suite.serialise_request_table_with_index()
   local argtab = setmetatable({}, { __index = {} })
   local req = request.request("TAG", "METH", argtab)
   local tab = assert(loadstring("return {" .. req .. "}"))()
   assert(tab.object == "TAG", "object tag not passed")
   assert(tab.method == "METH", "method name not passed")
   assert(tab.args.n == 1, "argument count not passed")
   assert(tab.args[1].methods[1] == "__index", "metamethod didn't pass")
   -- Best check that the received arg unwraps nicely
   assert(objects.receive(tab.args[1]) == argtab, "arg did not unwrap")
end

function suite.serialise_response_simple()
   local req = request.response("foo",12)
   local tab = assert(loadstring("return {" .. req .. "}"))()
   assert(tab.error == false, "error wasn't false")
   assert(type(tab.results) == "table", "results were missing")
   assert(tab.results.n == 2, "Result count not passed")
   assert(tab.results[1] == "foo", "result 1 not passed")
   assert(tab.results[2] == 12, "result 2 not passed")
end

function suite.serialise_response_table()
   local argtab = {}
   local req = request.response(argtab)
   local tab = assert(loadstring("return {" .. req .. "}"))()
   assert(tab.error == false, "error wasn't false")
   assert(type(tab.results) == "table", "results were missing")
   assert(tab.results.n == 1, "Result count not passed")
   assert(type(tab.results[1]) == "table", "result 1 not passed")
   assert(objects.receive(tab.results[1]) == argtab,
	  "arg did not unwrap")
end

function suite.deserialise()
   local argtab = {}
   local req = request.response(argtab)
   local tab = request.deserialise(req)
   assert(tab.error == false, "error wasn't false")
   assert(type(tab.results) == "table", "results were missing")
   assert(tab.results.n == 1, "Result count not passed")
   assert(type(tab.results[1]) == "table", "result 1 not passed")
   assert(tab.results[1] == argtab, "arg did not unwrap")
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
