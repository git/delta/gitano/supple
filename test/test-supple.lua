-- test/test-supple.lua
--
-- Supple - Tests for the core module
--
-- Copyright 2012 Daniel Silverstone <dsilvers@digital-scurf.org>
--
-- For Licence terms, see COPYING
--

-- Step one, start coverage

pcall(require, 'luacov')

local capi = require 'supple.capi'
local request = require 'supple.request'
local objects = require 'supple.objects'
local sandbox = require 'supple.sandbox'
local supple = require 'supple'

local testnames = {}

local real_assert = assert
local total_asserts = 0
local function assert(...)
   local retval = real_assert(...)
   total_asserts = total_asserts + 1
   return retval
end

local function add_test(suite, name, value)
   rawset(suite, name, value)
   testnames[#testnames+1] = name
end

local suite = setmetatable({}, {__newindex = add_test})

function suite.capi_passed()
   assert(supple.capi == capi, "Supple's capi entry is not supple.capi")
end

function suite.request_passed()
   assert(supple.request == request,
	  "Supple's request entry is not supple.request")
end

function suite.objects_passed()
   assert(supple.objects == objects,
	  "Supple's objects entry is not supple.objects")
end

function suite.sandbox_passed()
   assert(supple.sandbox == sandbox,
	  "Supple's sandbox entry is not supple.sandbox")
end

local count_ok = 0
for _, testname in ipairs(testnames) do
--   print("Run: " .. testname)
   local ok, err = xpcall(suite[testname], debug.traceback)
   if not ok then
      print(err)
      print()
   else
      count_ok = count_ok + 1
   end
end

print(tostring(count_ok) .. "/" .. tostring(#testnames) .. " [" .. tostring(total_asserts) .. "] OK")

os.exit(count_ok == #testnames and 0 or 1)
